import { Pokemon_In_Deck, User } from './model'
import { register } from './routes'
import UserRepository from './userRepository'
import users from './users.json'

const userRepository = new UserRepository()

const listUsers = () => {
  return userRepository.getAllUsers()
}

const listUsersScore = () => {
  return userRepository.getAllUsersScore()
}

const addUser = (newUser: User) => {
  if(userRepository.getUserByUsername(newUser.username) === undefined) {
    userRepository.createUser(newUser.username, newUser.password)
    return userRepository.getAllUsers()
  }
  return "This username already exists."
}

const findUser = (id: number) => {
  return userRepository.getUserById(id)
}

const connectUser = (user: User) => {
  if(userRepository.getUserByUsernameAndPassword(user.username, user.password) !== undefined) {
    return "You are connected " + user.username;
  }
  return "Wrong username or password"
}

const updateDeck = (userId: number, pokemonsInDeck: Pokemon_In_Deck[]) => {

}

export { listUsers, addUser, findUser, connectUser, listUsersScore, updateDeck }
