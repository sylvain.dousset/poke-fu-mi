export type User = {
    username: string;
    password: string;
    score: number;
}

export type UserFull = {
    userInfo: User;
    deck: Deck;
}

export type Deck = {
    pokemons: Pokemon[];
}

export type Pokemon_In_Deck = {
    id: number;
    position: number;
}

export type Pokemon = {
    
  }