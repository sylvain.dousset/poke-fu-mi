import * as express from "express"
import * as UserController from "./userController"
import { Pokemon_In_Deck, User } from './model'

export const register = (app: express.Application) => {
  
  // app.get('/', (req, res) => res.send('Hello World!'));

  app.get('/user', (req, res) => {
    res.status(200).json(UserController.listUsers())
  })

  app.get('/user/score', (req, res) => {
    res.status(200).json(UserController.listUsersScore())
  })

  app.get('/user/:id', (req, res) => {
    const userId: number = parseFloat(req.params.id)
    res.status(200).json(UserController.findUser(userId))
  })

  app.post('/user/login', (req, res) => {
    const newUser: User = req.body
    res.status(200).json(UserController.connectUser(newUser))
  })
 
  app.post('/user', (req, res) => {
    const newUser: User = req.body
    res.status(200).json(UserController.addUser(newUser))
  })

  app.put('/user/:id/deck', (req, res) => {
    const userId: number = parseFloat(req.params.id)
    const pokemonsInDeck: Pokemon_In_Deck[] = req.body
    res.status(200).json(UserController.updateDeck(userId, pokemonsInDeck))
  })

}
