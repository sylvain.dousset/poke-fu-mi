import * as express from "express"
import * as MatchController from "./matchController"
import { Match } from "./model";

export const register = (app: express.Application) => {
  
  // app.get('/', (req, res) => res.send('Hello World!'));

  app.get('/match', (req, res) => {
    res.status(200).json(MatchController.listMatches())
  });

  app.get('/match/:id', (req, res) => {
    const matchId: number = parseFloat(req.params.id)
    res.status(200).json(MatchController.findMatch(matchId))
  });

  app.post('/match', (req, res) => {
    const matchInfo: Match = req.body
    res.status(200).json(MatchController.createMatch(matchInfo));
  });

  app.get('/match/invitations/:id', (req, res) => {
    const userId: number = parseFloat(req.params.id)
    res.status(200).json(MatchController.findInvitePendingMatchByUserId(userId));
  });

  app.put('/match', (req, res) => {
    const matchInfo: Match = req.body
    res.status(200).json(MatchController.updateMatch(matchInfo));
  });

}
