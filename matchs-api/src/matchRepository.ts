import Database from 'better-sqlite3'
import fs from 'fs'
import { Match, MatchState, Match_User_Pokemon } from './model';

export default class MatchRepository {
  db: Database.Database

  constructor() {
    this.db = new Database('db/matches.db', { verbose: console.log });
    this.applyMigrations()    
  }

  getAllMatches(): Match[] {
    const statement = this.db.prepare("SELECT * FROM matches")
    const rows: Match[] = statement.all()
    return rows
  }

  getMatchById(matchId: number): Match {
    const statement = this.db
      .prepare("SELECT * FROM matches WHERE match_id = ?");
    const rows: Match = statement.get(matchId);
    return rows;
  }

  getAllPokemonsUsedInMatchByUser(matchId: number, userId: number): Match_User_Pokemon[] {
    const statement = this.db
      .prepare("SELECT * FROM match_user_pokemon WHERE match_id = ? AND user_id = ?");
    const rows: Match_User_Pokemon[] = statement.all(matchId, userId);
    return rows;
  }

  createMatch(user1Id: number, user2Id: number) {
    const statement =
      this.db.prepare("INSERT INTO matches (user1Id, user2Id, state) VALUES (?, ?, ?)");
    return statement.run(user1Id, user2Id, MatchState.PendingRequest).lastInsertRowid;
  }

  findInvitePendingMatchByUserId(userId: number): Match[] {
    const statement = this.db
      .prepare("SELECT * FROM matches WHERE user2Id = ? AND state = ?");
    const rows: Match[] = statement.all(userId, MatchState.PendingRequest);
    return rows;
  }

  updateMatch(matchId: number, user1Id: number, user2Id: number, state: MatchState, winnerId: number) {
    const statement =
      this.db.prepare("UPDATE matches SET user1Id = ?, user2Id = ?, state = ?, winnerId = ? WHERE match_id = ?");
    return statement.run(user1Id, user2Id, state, winnerId, matchId);
  }

  applyMigrations(){
    const applyMigration = (path: string) => {
      const migration = fs.readFileSync(path, 'utf8')
      this.db.exec(migration)
    }
    
    const testRow = this.db.prepare("SELECT name FROM sqlite_schema WHERE type = 'table' AND name = 'matches'").get()

    if (!testRow){
      console.log('Applying migrations on DB matches...')
      const migrations = ['db/migrations/init.sql'] 	 
      migrations.forEach(applyMigration)
    }
  }
}
