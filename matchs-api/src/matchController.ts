import MatchRepository from './matchRepository'
import { Match, MatchFull, Match_User_Pokemon } from './model'

const matchRepository = new MatchRepository()


const listMatches = () => {
  return matchRepository.getAllMatches()
}

const findMatch = (matchId: number) => {
  let matchInfo: Match = matchRepository.getMatchById(matchId);
  let pokeUser1 = matchRepository.getAllPokemonsUsedInMatchByUser(matchId, matchInfo.user1Id);
  let pokeUser2 = matchRepository.getAllPokemonsUsedInMatchByUser(matchId, matchInfo.user2Id);
  return new MatchFull(matchInfo, pokeUser1, pokeUser2);
}

const createMatch = (matchInfo: Match) => {
  return matchRepository.createMatch(matchInfo.user1Id, matchInfo.user2Id);
}

const findInvitePendingMatchByUserId = (userId: number) => {
  return matchRepository.findInvitePendingMatchByUserId(userId);
}

const updateMatch = (matchInfo: Match) => {
  return matchRepository.updateMatch(matchInfo.id, matchInfo.user1Id, matchInfo.user2Id, matchInfo.state, matchInfo.winnerId);
}

export { listMatches, findMatch, createMatch, findInvitePendingMatchByUserId, updateMatch }
