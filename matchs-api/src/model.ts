export enum MatchState {
  PendingRequest = "PENDING_REQUEST",
  OnGoing = "ON_GOING",
  Started = "STARTED",
  Finished = "FINISHED",
}

export type Match = {
  id: number;
  user1Id: number;
  user2Id: number;
  state: MatchState;
  winnerId: number;
}

export class MatchFull {
  matchInfo: Match;
  pokemonsUsedByUser1: Match_User_Pokemon[];
  pokemonsUsedByUser2: Match_User_Pokemon[];

  constructor(matchInfo: Match, pokemonsUsedByUser1: Match_User_Pokemon[], pokemonsUsedByUser2: Match_User_Pokemon[]) {
    this.matchInfo = matchInfo;
    this.pokemonsUsedByUser1 = pokemonsUsedByUser1;
    this.pokemonsUsedByUser2 = pokemonsUsedByUser2;
  }
}

export type Match_User_Pokemon = {
  pokemon_id: number;
  match_id: number;
  user_id: number;
  round: number
}

export type Pokemon = {
  name: string;
  types: string[];
  image1: string;
}