"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const better_sqlite3_1 = __importDefault(require("better-sqlite3"));
const fs_1 = __importDefault(require("fs"));
const model_1 = require("./model");
class MatchRepository {
    constructor() {
        this.db = new better_sqlite3_1.default('db/matches.db', { verbose: console.log });
        this.applyMigrations();
    }
    getAllMatches() {
        const statement = this.db.prepare("SELECT * FROM matches");
        const rows = statement.all();
        return rows;
    }
    getMatchById(matchId) {
        const statement = this.db
            .prepare("SELECT * FROM matches WHERE match_id = ?");
        const rows = statement.get(matchId);
        return rows;
    }
    getAllPokemonsUsedInMatchByUser(matchId, userId) {
        const statement = this.db
            .prepare("SELECT * FROM match_user_pokemon WHERE match_id = ? AND user_id = ?");
        const rows = statement.all(matchId, userId);
        return rows;
    }
    createMatch(user1Id, user2Id) {
        const statement = this.db.prepare("INSERT INTO matches (user1Id, user2Id, state) VALUES (?, ?, ?)");
        return statement.run(user1Id, user2Id, model_1.MatchState.PendingRequest).lastInsertRowid;
    }
    findInvitePendingMatchByUserId(userId) {
        const statement = this.db
            .prepare("SELECT * FROM matches WHERE user2Id = ? AND state = ?");
        const rows = statement.all(userId, model_1.MatchState.PendingRequest);
        return rows;
    }
    updateMatch(matchId, user1Id, user2Id, state, winnerId) {
        const statement = this.db.prepare("UPDATE matches SET user1Id = ?, user2Id = ?, state = ?, winnerId = ? WHERE match_id = ?");
        return statement.run(user1Id, user2Id, state, winnerId, matchId);
    }
    applyMigrations() {
        const applyMigration = (path) => {
            const migration = fs_1.default.readFileSync(path, 'utf8');
            this.db.exec(migration);
        };
        const testRow = this.db.prepare("SELECT name FROM sqlite_schema WHERE type = 'table' AND name = 'matches'").get();
        if (!testRow) {
            console.log('Applying migrations on DB matches...');
            const migrations = ['db/migrations/init.sql'];
            migrations.forEach(applyMigration);
        }
    }
}
exports.default = MatchRepository;
//# sourceMappingURL=matchRepository.js.map