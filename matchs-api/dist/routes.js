"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.register = void 0;
const MatchController = __importStar(require("./matchController"));
const register = (app) => {
    // app.get('/', (req, res) => res.send('Hello World!'));
    app.get('/match', (req, res) => {
        res.status(200).json(MatchController.listMatches());
    });
    app.get('/match/:id', (req, res) => {
        const matchId = parseFloat(req.params.id);
        res.status(200).json(MatchController.findMatch(matchId));
    });
    app.post('/match', (req, res) => {
        const matchInfo = req.body;
        res.status(200).json(MatchController.createMatch(matchInfo));
    });
    app.get('/match/invitations/:id', (req, res) => {
        const userId = parseFloat(req.params.id);
        res.status(200).json(MatchController.findInvitePendingMatchByUserId(userId));
    });
    app.put('/match', (req, res) => {
        const matchInfo = req.body;
        res.status(200).json(MatchController.updateMatch(matchInfo));
    });
};
exports.register = register;
//# sourceMappingURL=routes.js.map