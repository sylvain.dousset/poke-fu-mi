"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.MatchFull = exports.MatchState = void 0;
var MatchState;
(function (MatchState) {
    MatchState["PendingRequest"] = "PENDING_REQUEST";
    MatchState["OnGoing"] = "ON_GOING";
    MatchState["Started"] = "STARTED";
    MatchState["Finished"] = "FINISHED";
})(MatchState = exports.MatchState || (exports.MatchState = {}));
class MatchFull {
    constructor(matchInfo, pokemonsUsedByUser1, pokemonsUsedByUser2) {
        this.matchInfo = matchInfo;
        this.pokemonsUsedByUser1 = pokemonsUsedByUser1;
        this.pokemonsUsedByUser2 = pokemonsUsedByUser2;
    }
}
exports.MatchFull = MatchFull;
//# sourceMappingURL=model.js.map