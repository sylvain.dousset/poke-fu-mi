"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.updateMatch = exports.findInvitePendingMatchByUserId = exports.createMatch = exports.findMatch = exports.listMatches = void 0;
const matchRepository_1 = __importDefault(require("./matchRepository"));
const model_1 = require("./model");
const matchRepository = new matchRepository_1.default();
const listMatches = () => {
    return matchRepository.getAllMatches();
};
exports.listMatches = listMatches;
const findMatch = (matchId) => {
    let matchInfo = matchRepository.getMatchById(matchId);
    let pokeUser1 = matchRepository.getAllPokemonsUsedInMatchByUser(matchId, matchInfo.user1Id);
    let pokeUser2 = matchRepository.getAllPokemonsUsedInMatchByUser(matchId, matchInfo.user2Id);
    return new model_1.MatchFull(matchInfo, pokeUser1, pokeUser2);
};
exports.findMatch = findMatch;
const createMatch = (matchInfo) => {
    return matchRepository.createMatch(matchInfo.user1Id, matchInfo.user2Id);
};
exports.createMatch = createMatch;
const findInvitePendingMatchByUserId = (userId) => {
    return matchRepository.findInvitePendingMatchByUserId(userId);
};
exports.findInvitePendingMatchByUserId = findInvitePendingMatchByUserId;
const updateMatch = (matchInfo) => {
    return matchRepository.updateMatch(matchInfo.id, matchInfo.user1Id, matchInfo.user2Id, matchInfo.state, matchInfo.winnerId);
};
exports.updateMatch = updateMatch;
//# sourceMappingURL=matchController.js.map