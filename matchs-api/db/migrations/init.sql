CREATE TABLE IF NOT EXISTS matches (
  match_id INTEGER PRIMARY KEY,
  user1Id INTEGER NOT NULL,
  user2Id	INTEGER NOT NULL,
  state	TEXT NOT NULL,
  winnerId INTEGER
);

CREATE TABLE IF NOT EXISTS match_user_pokemon (
  id INTEGER PRIMARY KEY,
  pokemon_id INTEGER NOT NULL,
  match_id INTEGER NOT NULL,
  user_id INTEGER NOT NULL,
  round INTEGER NOT NULL
);

INSERT INTO matches (user1Id, user2Id , state, winnerId) VALUES ( 1,	2 , 'FINISHED', 1);

-- ROUND 1
INSERT INTO match_user_pokemon (pokemon_id, match_id , user_id, round) VALUES ( 410,	1 , 1, 1);
INSERT INTO match_user_pokemon (pokemon_id, match_id , user_id, round) VALUES ( 35,	1 , 2, 1);

-- ROUND 2
INSERT INTO match_user_pokemon (pokemon_id, match_id , user_id, round) VALUES ( 491,	1 , 1, 2);
INSERT INTO match_user_pokemon (pokemon_id, match_id , user_id, round) VALUES ( 493,	1 , 2, 2);


